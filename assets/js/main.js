$(document).ready(function () {
	//search
	openSearch();
	topmobileMenu();
	navFixed();
	mobileMenu();
	smoothScroll();
	scheduleTab();
	openSchedule();
	
});

//SEARCH ICON 
function openSearch() {
	$('.searchIcon').click(function() {
		var parent = $(this).parent();
		if(!$(parent).hasClass('open')) {
			$(this).parent().addClass('open');
		} else {
			$(this).parent().removeClass('open');
		}
	});
}

//TOP MOBILE MENU
function topmobileMenu() {
	$('.topnav').on('click', function () {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$(this).children('i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
			$('.topbar').removeClass('slideDown');
		} else {
			$(this).addClass('open');
			$(this).children('i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
			$('.topbar').addClass('slideDown');
			
		}
	});
}

//FIXED NAV WHEN SCROLLED
function navFixed() {
	$(window).on('scroll',function () {
		if ($(this).scrollTop() > 110) {
			$('header.main').addClass('fixed');
		} else if ($(this).scrollTop() < 110) {
			$('header.main').removeClass('fixed');
		}
	});
}

//MOBILE MENU
function mobileMenu() {
	var $nav = $('.main-nav');

	$('.burgermenu').click(function() {
		if(!$(this).hasClass('open')) {
			$(this).addClass('open');
			$nav.addClass('show');
		} else {
			$(this).removeClass('open');
			$nav.removeClass('show');
		}
	});
}

function smoothScroll() {
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html,body').animate({
			  scrollTop: target.offset().top - 50
			}, 1000);
			return false;
		  }
		}
	});
}

function scheduleTab(){
	$('.shedpanel h1').each(function(){
		$(this).on('click',function(){
			var tabname = $(this).attr('class');
			if($(this).hasClass('active')) {
				return false;
			} else {
				$(this).addClass('active').siblings('h1').removeClass('active');
				$('#' + tabname).addClass('active').siblings('div').removeClass('active');
				
			}
			
		});
	});
}

function openSchedule() {
	$('.fullsched-btn').on('click',function(){
		if($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('#panel').removeClass('open');
			$('.expand').html('EXPAND <span><i class="fa fa-chevron-down"/></span>');
		} else {
			$(this).addClass('open');
			$('#panel').addClass('open');
			$('.expand').html('COLLAPSE <span><i class="fa fa-chevron-up"/></span>');
		}
	});
}
